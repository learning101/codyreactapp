import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        DevOps 101
        <a
          className="App-link"
          href="https://yacinesoufiane.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          by Yacine Soufiane
        </a>
      </header>
    </div>
  );
}

export default App;
